[[extend-theme-install]]

=== Télécharger et installer un thème depuis _Drupal.org_

[role="summary"]
Comment télécharger et installer un thème depuis Drupal.org.

(((Thème,télécharger)))
(((Thème,installer)))
(((Thème,activer)))
(((Thème,contribué)))
(((Thème,personnalisé)))
(((Thème contribué,télécharger)))
(((Thème contribué,installer)))
(((Thème contribué,activer)))
(((Thème personnalisé,installer)))
(((Thème personnalisé,activer)))
(((Télécharger,thème)))
(((Installer,thème)))
(((Activer,thème)))
(((Outil Drush,utiliser pour installer un thème)))
(((Drupal.org,télécharger et installer un thème depuis le site web)))

==== Objectif

Télécharger et installer un thème depuis _Drupal.org_.

==== Prérequis

* <<extend-theme-find>>
* <<install-tools>>

==== Prérequis du site

Composer doit être installé pour télécharger des thèmes. Si vous voulez utiliser
Drush, procéder tout d'abord à son installation. Consulter <<install-tools>>.

==== Étapes

Pour installer un thème contribué, commencer par le télécharger avec Composer.
Ensuite, l'installer en utilisant soit l'interface d'administration, soit Drush.
Si vous installez un thème personnalisé plutôt qu'un thème contribué, qui n'est
pas disponible via Composer, passer ces étapes concernant le téléchargement du
module, et se référer à <<extend-manual-install>>. Revenir alors ici et suivre
les étapes pour installer le module, en utilisant soit l'interface
d'administration, soit Drush.

===== Télécharger le thème contribué avec Composer

. Sur la page de projet du thème sur drupal.org (par exemple
_https://www.drupal.org/project/honey_), dérouler la page jusqu'à la section
_Releases_ (versions) en bas de page.

. Copier la commande Composer fournie correspondant à la version du thème que
vous souhaitez installer.
+
--
// Releases section of a theme project page on drupal.org.
image:images/extend-theme-install-download.png["Trouver la commande
d'installation d'un thème avec Composer"]
--

. Vous pouvez autrement taper la commande suivante (en remplaçant le nom
machine du thème et la version souhaitée) :
+
----
composer require 'drupal/honey:^1.0'
----

. En ligne de commande, se placer dans le répertoire racine du projet. Coller la
commande Composer et l'exécuter.

. Vous devriez voir un message indiquant que le thème a été téléchargé avec
succès.

===== Installer le thème depuis l'interface d'administration

. Dans le menu d'administration _Gérer_, naviguer vers _Apparence_
(_admin/appearance_). La page _Apparence_ apparaît.

. Localiser le nouveau thème sous _Thèmes désinstallés_ et cliquer sur
_Installer et définir par défaut_ pour l'utiliser. Toutes les pages hors celles
d'administration de votre site utiliseront alors ce nouveau thème.
+
--
// Honey theme on the Appearance page.
image:images/extend-theme-install-appearance-page.png["Thèmes désinstallés sur la page Apparence",width="286px"]
--

===== Utiliser Drush pour installer un thème

. Pour installer le thème et le définir comme thème par défaut, exécuter les
commandes Drush suivantes, en donnant le nom du projet en paramètre (par
exemple, _honey_) :
+
----
drush theme:enable honey
drush config:set system.theme default honey
----

. Suivre les instructions à l'écran.

==== Pour approfondir

* Dans le menu d'administration _Gérer_, naviguer vers _Apparence_
(_admin/appearance) et désinstaller tous les thèmes que vous n'utilisez pas.

* <<extend-module-find>>

* <<extend-module-install>>

* Si vous ne voyez pas d'effet de ces modifications sur votre site, il pourrait
être nécessaire de vider le cache. Consulter <<prevent-cache-clear>>.


// ==== Related concepts

==== Vidéos (en anglais)

// Video from Drupalize.Me.
video::https://www.youtube-nocookie.com/embed/UFgddj0F_bU[title="Downloading and Installing a Theme from Drupal.org"]

//==== Additional resources


*Attributions*

Écrit et modifié par https://www.drupal.org/u/eojthebrave[Joe Shindelar] de
https://drupalize.me[Drupalize.Me], et https://www.drupal.org/u/batigolix[Boris
Doesborgh]. Traduit par https://www.drupal.org/u/fmb[Felip Manyer i Ballester].
